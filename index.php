<?php

require "config.php";
require "normalize-date.php";

/*
 * Parse input, if any.
 */

$start_date = normalizeDate(isset($_REQUEST["start_date"]) ? $_REQUEST["start_date"] : null,
    "m/d/Y",
    date("Y-01-01"),
    "Y-m-d");
$end_date = normalizeDate(isset($_REQUEST["end_date"]) ? $_REQUEST["end_date"] : null,
    "m/d/Y",
    date("Y-m-d"),
    "Y-m-d");
$start_date_mdy = normalizeDate($start_date, "Y-m-d", null, "m/d/Y");
$end_date_mdy = normalizeDate($end_date, "Y-m-d", null, "m/d/Y");

/*
 * Gather data.
 */

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$statement = $dbh->prepare(<<<'SQL'
    SELECT sales.sku,
           sales.name,
           CAST(stock.qty AS UNSIGNED) qty,
           attr.value store,
           CAST(SUM(sales.qty_invoiced) AS UNSIGNED) invoiced,
           CAST(SUM(sales.qty_ordered) AS UNSIGNED) ordered,
           CAST(SUM(sales.qty_refunded) AS UNSIGNED) refunded,
           CAST(SUM(sales.qty_shipped) AS UNSIGNED) shipped,
           CAST(SUM(sales.qty_canceled) AS UNSIGNED) canceled
    FROM sales_flat_order_item sales
    LEFT JOIN cataloginventory_stock_item stock ON stock.product_id = sales.product_id
    LEFT JOIN catalog_product_entity_int entity ON entity.entity_id = sales.product_id
    LEFT JOIN eav_attribute_option_value attr ON attr.option_id = entity.value
    WHERE sales.product_type <> 'configurable'
          AND sales.created_at BETWEEN :start_date AND :end_date
          AND entity.attribute_id = 178
          AND entity.entity_type_id = 4
          AND attr.option_id BETWEEN 48 AND 51
    GROUP BY store, sku, name
    ORDER BY store, sku, name
SQL
);

$statement->execute(
    array(
        ":start_date" => $start_date,
        ":end_date" => $end_date
    )
);

$rows = $statement->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Spirit Store Reports</title>
<link type="text/css" rel="stylesheet" href="static/css/main.css" />
<link type="text/css" rel="stylesheet" href="static/css/bootstrap-3.1.1-dist.min.css" />
<link type="text/css" rel="stylesheet" href="static/css/bootstrap-theme-3.1.1-dist.min.css" />
<link type="text/css" rel="stylesheet" href="static/css/dataTables.tableTools-2.2.4.min.css" />
<link type="text/css" rel="stylesheet" href="static/css/dataTables.bootstrap-1.10.11.css" />
<link type="text/css" rel="stylesheet" href="static/css/jquery-ui-1.12.1-custom.min.css" />
<script type="text/javascript" src="static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="static/js/bootstrap-3.1.1-dist.min.js"></script>
<script type="text/javascript" src="static/js/jquery.dataTables-1.10.11.min.js"></script>
<script type="text/javascript" src="static/js/dataTables.tableTools-2.2.4.min.js"></script>
<script type="text/javascript" src="static/js/dataTables.bootstrap-1.10.11.min.js"></script>
<script type="text/javascript" src="static/js/jquery-ui-1.12.1-custom.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#data_table').DataTable({
            "iDisplayLength": 10,
            "bAutoWidth": true,
            "bSaveState": true,
            "bSort": true,
            "sDom": 'T<"clear">lfrtip',
            "oTableTools": {
                "sRowSelect": "multi",
                "aButtons": [
                    "select_none",
                    "select_all",
                    {
                        "sExtends": "copy",
                        "bSelectedOnly": true
                    },
                    {
                        "sExtends": "xls",
                        "bSelectedOnly": true
                    },
                    {
                        "sExtends": "print",
                        "bSelectedOnly": true
                    }
                ],
                "sSwfPath": "static/swf/TableTools-2.2.4-copy_csv_xls.swf"
            },
            "oLanguage": {
                "sSearch": "Filter:"
            }
        });

        $('#data_table tr').click(function() {
            $(this).toggleClass('row_selected');
        });

        $("#start_date, #end_date").datepicker({
            dateFormat: "mm/dd/yy"
        });
    });
</script>
</head>
<body>
<header>
    <img src="static/images/shg-logo.png" width="200px" />
    Spartan Spirit Shop Product Sales Inquiry
</header>
<form id="date_form" method="post">
    <label for="start_date">Starting date:</label>
    <input name="start_date" id="start_date" type="text" value="<?= $start_date_mdy ?>" />
    <label for="end_date">Ending date:</label>
    <input name="end_date" id="end_date" type="text" value="<?= $end_date_mdy ?>" />
    <input type="submit" value="Get sales data" />
</form>
<?php
if ($rows):
?>
<table id="data_table" class="table table-striped table-bordered dataTable" role="grid">
    <thead>
        <tr>
            <th>Store</th>
            <th>SKU</th>
            <th>Product</th>
            <th>Stock</th>
            <th>Invoiced</th>
            <th>Shipped</th>
            <th>Canceled</th>
            <th>Ordered</th>
            <th>Refunded</th>
        </tr>
    </thead>
    <tbody>
<?php
    foreach ($rows as $row):
?>
        <tr>
            <td><?= $row['store'] ?></td>
            <td><?= $row['sku'] ?></td>
            <td><?= $row["name"] ?></td>
            <td><?= $row["qty"] ?></td>
            <td><?= $row["invoiced"] ?></td>
            <td><?= $row["shipped"] ?></td>
            <td><?= $row["canceled"] ?></td>
            <td><?= $row["ordered"] ?></td>
            <td><?= $row["refunded"] ?></td>
        </tr>
<?php
    endforeach;
?>
    </tbody>
    <tfoot>
        <tr>
            <th>Store</th>
            <th>SKU</th>
            <th>Product</th>
            <th>Stock</th>
            <th>Invoiced</th>
            <th>Shipped</th>
            <th>Canceled</th>
            <th>Ordered</th>
            <th>Refunded</th>
        </tr>
    </tfoot>
</table>
<?php
endif;
?>
</body>
</html>
