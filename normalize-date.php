<?php

/**
 * Normalize and verify a formatted date.
 *
 * @param string $input The date to check.
 * @param string $inFormat Format for parsing date. The format is the same as date()'s format parameter.
 * @param mixed $default Value to return if date doesn't parse.
 * @param string|null $outFormat Format for returning date. Defaults to the same as $inFormat. The format is the same as
 *                    date()'s format parameter.
 * @return string|mixed Returns the normalized date on success or $default.
 */
function normalizeDate($input, $inFormat, $default = null, $outFormat = null) {
    $dateTime = DateTime::createFromFormat($inFormat, $input);
    if (!$dateTime) {
        return $default;
    } else {
        return $dateTime->format($outFormat ? $outFormat : $inFormat);
    }
}
